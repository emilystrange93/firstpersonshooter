# PEC2 - First Person Shooter
## Mel Aubets

En esta práctica se han creado tres escenas, una inicial en terreno montañoso, una intermedia en una taberna y una final de Game Over.
Para lograr los distintos objetivos propuestos se han creado distintos scripts, se explica su funcionamiento de forma esquemática a continuación.

### Scripts

- **Restart Manager:** Se encarga de gestionar el botón de reinicio de la pantalla Game Over.
- **Weapon Scripts:**
    - **Gun System:** Este Script gestiona las armas mediante distintas variables y dispara con RayCast. También se han añadido algunos efectos visuales y sonoros.
    - **Weapon Switching:** Asignará un índice a los distintos hijos de _Weapon Holder_  para permitir el cambio de arma mediante la ruedecita del ratón.
- **IA Scripts:**
    - **Enemy AI:** Máquina de estados que gestionará los tres estados del enemigo: __Roaming, Chasing, Attack__.
    - **Field Of View y Field Of View Editor:** El campo de visión que detectará los objetivos del enemigo mediante RayCast.
- **Health Scripts:**
    - **Enemy Health:** Gestiona la salud y la muerte del enemigo, así como el item que dejará al morir.
    - **Player Health:** Gestiona la salud y la muerte del jugador, además, gestiona los __triggers__.
- **Doors & Platforms Scripts:**
    - **Digit:** Asigna un dígito a las dianas del puzle.
    - **Door:** Abre las puertas mediante el booleano  __isOpening__.
    - **Enemy Check:** Controla si quedan enemigos para abrir la puerta de la primera escena.
    - **Platform Attach:** Hace que el jugador sea hijo de la plataforma para que le acompañe en el movimiento.
    - **Puzzle Manager:** Gestiona los índices de las dianas del puzle de la segunda escena.

### [Vídeo Demostrativo](https://youtu.be/pieBaKyXqSI)

### Assets Utilizados en esta PEC:
- [Standard Assets](https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2018-4-32351)
- [Unity Particle Pack 5.x](https://assetstore.unity.com/packages/essentials/asset-packs/unity-particle-pack-5-x-73777)
- [12x70 Rem Ammo Box](https://assetstore.unity.com/packages/3d/props/weapons/12x70-rem-ammo-box-193342)
- [South Pacific Village](https://assetstore.unity.com/packages/3d/environments/south-pacific-village-198032)
- [Free Laser Weapons](https://assetstore.unity.com/packages/audio/sound-fx/weapons/free-laser-weapons-214929)
- [Military Target](https://assetstore.unity.com/packages/3d/environments/military-target-136071)
- [Bubble Font (free version)](https://assetstore.unity.com/packages/2d/fonts/bubble-font-free-version-24987)
- [Low Poly Combat Drone](https://assetstore.unity.com/packages/3d/low-poly-combat-drone-82234)
- [Pirate Tavern](https://assetstore.unity.com/packages/3d/environments/fantasy/pirate-tavern-113463)
- [Weapons Soldiers Sounds Pack](https://assetstore.unity.com/packages/audio/sound-fx/weapons/weapon-soldier-sounds-pack-29662)
- [First Aid Set](https://assetstore.unity.com/packages/3d/props/first-aid-set-160073)
- [Sci-Fi Weapons](https://devassets.com/assets/sci-fi-weapons/)

### Tutoriales Utilizados en esta PEC:
- [HOW TO MAKE AN FPS GAME IN UNITY FOR FREE](https://www.youtube.com/playlist?list=PLZ1b66Z1KFKjsq3WTBBkRMUZqzOAgIM4V)
- [SHOOTING with BULLETS + CUSTOM PROJECTILES || Unity3D Tutorial (#1)](https://www.youtube.com/watch?v=wZ2UUOC17AY&t=38s&ab_channel=Dave%2FGameDevelopment)
- [SHOOTING with BULLETS + CUSTOM PROJECTILES || Unity 3D Tutorial (#2)](https://www.youtube.com/watch?v=0jGL5_DFIo8&ab_channel=Dave%2FGameDevelopment)
- [How to make ALL kinds of GUNS with just ONE script! (Unity3d tutorial)](https://www.youtube.com/watch?v=bqNW08Tac0Y&ab_channel=Dave%2FGameDevelopment)
- [Shooting with Raycasts - Unity Tutorial](https://www.youtube.com/watch?v=THnivyG0Mvo&t=638s&ab_channel=Brackeys)
- [Weapon Switching - Unity Tutorial](https://www.youtube.com/watch?v=Dn_BUIVdAPg&t=480s&ab_channel=Brackeys)
- [Simple Enemy AI in Unity (State Machine, Find Target, Chase, Attack)](https://www.youtube.com/watch?v=db0KWYaWfeM&t=776s&ab_channel=CodeMonkey)
- [Simple 3D Text in Unity](https://www.youtube.com/watch?v=H-IXhTOhKPI&ab_channel=SpeedTutor)