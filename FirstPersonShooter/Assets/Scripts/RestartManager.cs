using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartManager : MonoBehaviour
{
    public Button restart;

    private void Update()
    {
        restart.onClick.AddListener(Restart);
    }

    void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
