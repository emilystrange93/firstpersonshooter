using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    [Range(1f,100f)]
    public float health = 100f;
    [Range(1f,100f)]
    public float shield = 100f;
    public Transform weapons;
    public ProgressBar healthBar;
    public ProgressBar shieldBar;

    private void Start()
    {
        SetHealthAndShield();
    }

    private void Update()
    {
        SetHealthAndShield();
    }

    public void TakeDamage(float damage)
    {
        if(shield > 0)
        {
            shield -= damage * 0.75f;
            health -= damage * 0.25f;
        }
        else
        {
            health -= damage;
        }

        if(health <= 0)
        {
            Dead();
        }

    }

    public void Dead()
    {
        SceneManager.LoadScene(2);
    }

    public void SetHealthAndShield()
    {
        healthBar.BarValue = health;
        shieldBar.BarValue = shield;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Life")
        {
            health = 100f;
            Destroy(other.gameObject);
        }
        if(other.tag == "Ammo")
        {
            int i = 0;
            foreach(Transform weapon in weapons)
            {
                if(weapon.gameObject.activeSelf == true)
                    weapon.GetComponent<GunSystem>().Reload();
                i++;
            }
            Destroy(other.gameObject);
        }
        if(other.tag == "Shield")
        {
            shield = 100F;
            Destroy(other.gameObject);

        }

        if (other.tag == "EndGame")
        {
            SceneManager.LoadScene(2);
        }

        if(other.tag == "NextScene")
        {
            SceneManager.LoadScene(1);
        }
    }

}
