using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public float health = 50f;
    public int rand;
    public GameObject[] items;

    public void TakeDamage(float damage)
    {
        health -= damage;
        if(health < 0f)
        {
            rand = Random.Range(0, 2);
            Instantiate(items[rand], transform.position, transform.rotation);
            Dead();
        }
    }

    private void Dead()
    {
        Destroy(gameObject);
    }
}
