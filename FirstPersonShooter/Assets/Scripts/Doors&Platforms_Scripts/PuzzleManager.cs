using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager: MonoBehaviour
{
    public Door door;

    private void Start()
    {
        int i = 0;
        foreach(Transform target in transform)
        {
            target.GetComponent<Digit>().digit = i;
            i++;
        }
    }

    private void Update()
    {
        GameObject[] military = GameObject.FindGameObjectsWithTag("Military");
        if (military.Length == 0)
        {
            door.isOpening = true;
        }
    }
}
