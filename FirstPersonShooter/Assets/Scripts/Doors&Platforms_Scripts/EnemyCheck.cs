using System.Collections;
using UnityEngine;

public class EnemyCheck : MonoBehaviour
{
    public Door door;
    // Update is called once per frame
    void Update()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        if(enemies.Length == 0)
        {
            Debug.Log("open");
            door.isOpening = true;
        }
    }
}
