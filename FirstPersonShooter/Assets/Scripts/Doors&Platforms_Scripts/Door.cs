using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Transform door;
    public Transform openLocation;

    public float speed = 1.0f;

    public bool isOpening = false;
    Vector3 distance;

    // Update is called once per frame
    void Update()
    {
        if (isOpening)
        {
            distance = door.localPosition - openLocation.localPosition;
            if (distance.magnitude < 0.001f)
            {
                isOpening = false;
                door.localPosition = openLocation.localPosition;
            }
            else
            {
                door.localPosition = Vector3.Lerp(door.localPosition, openLocation.localPosition, Time.deltaTime * speed);
                door.localRotation = Quaternion.Lerp(door.localRotation, openLocation.rotation, Time.deltaTime * speed);
            }
        }
    }
}
