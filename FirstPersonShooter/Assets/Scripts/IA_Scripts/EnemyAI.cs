using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    private enum State
    {
        Roaming,
        ChaseTarget,
        Attack,
    }
    private Vector3 startingPosition;
    public float walkRadius;

    public NavMeshAgent agent;
    public GameObject target;

    public float range = 50f;
    public float damage = 2f;
    private float nextShootTime;

    public ParticleSystem muzzleFlash;
    public AudioSource shootSound;

    public FieldOfView fow;

    private State state;

    private void Awake()
    {
        state = State.Roaming;
    }
    private void Start()
    {
        startingPosition = transform.position;
    }
    private void Update()
    {
        switch (state)
        {
            default:

            case State.Roaming:
                if (agent.remainingDistance <= agent.stoppingDistance)
                {
                    agent.SetDestination(GetRoamingPosition());
                }

                if (FindTarget())
                    state = State.ChaseTarget;
                break;
            case State.ChaseTarget:
                if (!FindTarget())
                    state = State.Roaming;
                else
                {
                    Vector3 lastPosition = fow.visibleTargets[0].position;
                    agent.SetDestination(lastPosition);

                    if (Vector3.Distance(agent.transform.position, target.transform.position) <= 30f)
                    {
                        state = State.Attack;
                    }
                }
                break;
            case State.Attack:
                if (Time.time > nextShootTime)
                {
                    agent.SetDestination(agent.transform.position);
                    transform.LookAt(target.transform);
                    Shoot();
                    float fireRate = 0.5f;
                    nextShootTime = Time.time + fireRate;
                }
                if (!FindTarget())
                    state = State.Roaming;
                break;
        }
    }

    private Vector3 GetRoamingPosition()
    {
        Vector3 finalPosition = Vector3.zero;
        Vector3 randomPosition = Random.insideUnitSphere * walkRadius;
        randomPosition += transform.position;
        if (NavMesh.SamplePosition(randomPosition, out NavMeshHit hit, walkRadius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }

    private bool FindTarget()
    {
        if (fow.visibleTargets.Count != 0)
        {
            return true;
        }

        return false;
    }

    private void Shoot()
    {
        muzzleFlash.Play();
        shootSound.Play();

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            PlayerHealth target = hit.transform.GetComponent<PlayerHealth>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }

        }
    }

}
