using UnityEngine;
using UnityEngine.UI;

public class GunSystem : MonoBehaviour
{
    //Gun stats
    public int damage;
    public float timeBetweenShooting, range, reloadTime;
    public int magazineSize;
    int bulletsLeft;
    int i;

    //bools 
    bool shooting, readyToShoot, reloading;

    //Reference
    public Camera fpsCam;
    public RaycastHit rayHit;
    public LayerMask whatIsEnemy;

    //Graphics
    public ParticleSystem muzzleFlash, impactEffect;
    public AudioSource shootSound, reloadSound;
    public Text ammo;

    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
        i = 0;
    }
    private void Update()
    {
        MyInput();

        if (ammo != null)
            ammo.text = bulletsLeft + " / " + magazineSize;
    }
    private void MyInput()
    {
        shooting = Input.GetKeyDown(KeyCode.Mouse0);

        //Shoot
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        readyToShoot = false;

        muzzleFlash.Play();
        shootSound.Play();

        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            EnemyHealth enemy = hit.transform.GetComponent<EnemyHealth>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }
            if (hit.transform.CompareTag("Military"))
            {
                Transform military = hit.transform;
                if(i == military.GetComponent<Digit>().digit)
                {
                    military.gameObject.SetActive(false);
                    i++;
                }
                else
                {
                    foreach(Transform child in military.parent)
                    {
                        child.gameObject.SetActive(true);
                    }
                    i = 0;
                }
            }

            GameObject instance = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal)).gameObject;
            Destroy(instance, 0.1f);
        }

        bulletsLeft--;

        Invoke("ResetShot", timeBetweenShooting);
    }
    private void ResetShot()
    {
        readyToShoot = true;
    }
    public void Reload()
    {
        reloading = true;
        reloadSound.Play();
        Invoke("ReloadFinished", reloadTime);
    }
    private void ReloadFinished()
    {
        bulletsLeft = magazineSize;
        reloading = false;
    }

}
